#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

/*
 * SIZE: 1700 1250
 * TIME: +/-20 min
 *
 */

#define META 128
#define HEADER_META "video custom binary format 0239"
#define image(x,y) pixels[x*width+y]

const char* header = HEADER_META; //asciiz
const char sfooter[] = {'f','r','a','m','e','s',' ','c','h','u','n','k'};

int main(void) {
	srand(time(NULL));
	long width, height;
	int x, y, i, max;

	FILE *out;

//	chdir(TEMP);
	out = fopen("movie.in", "wb");
if (out == NULL) {
perror("movie.in");
exit(EXIT_FAILURE);
	}

	width = rand() % 512 + 1536;
	height = rand() % 384 + 1152;
	width =  5; //para que no sea tan pesado, lo reduzco a 5 x 5  (1920X1440)
	height = 5;

  fwrite(&width, sizeof(width), 1, out); //8 bytes
  fwrite(&height, sizeof(height), 1, out); //8 bytes
  fflush(out); //se trabaja con buffers pero en algun momento se pasa a memoria, a un fichero

  int *pixels = (int*) malloc((height+2) * (width+2) * sizeof(int)); //5+2 * 5+2 * 4, ponemos un +2 para dejar un borde a la imagen
  int *meta = (int*) malloc(sizeof(int) * META); //128*4 (metadatos del video)
  memcpy(meta, header, strlen(header)); // rand, copiamos todo lo que habia en header en meta
  fwrite(meta, META * sizeof(int), 1, out); // escribimos a fichero  el valor de meta, (el 1 son las veces que quieres que se copie el argumento)
  memset(meta, 0, META); //
  memcpy(meta, sfooter, strlen(sfooter)); // copia en meta lo que hay en sfooter, como es ascii va buscando hasta encontrar un nulo (que sería un 0) y como no se lo hemos puesto, stack overflow

max = 10; //numero de imagenes que tiene nuestro video
//image es una funcion del preprocesador
for (i = 0; i < max; i++) {
for (y = 0; y <= height+1; y++) {
for (x = 0; x <= width+1; x++) {
		if ((x == 0) || (x == width+1) || (y == 0) || (y == height+1)) image(y,x) = 0;
                                else image(y,x) = rand() % 256; //hacemos que las imagenes sean un tipo de color, el modulo 256 son escalas de rojo
	}
}
   fwrite(pixels, (height+2) * (width+2) * sizeof(int), 1, out); //escribimos los pixels que acabamos de rellenar en la imagen
   fwrite(meta, (META >> 2) * sizeof(char), 1, out); //escribimos el meta (con el >> (shift shift) pasamos de 128 a 32)
   }

   fflush(out); 
   fclose(out);

   return EXIT_SUCCESS;
}
